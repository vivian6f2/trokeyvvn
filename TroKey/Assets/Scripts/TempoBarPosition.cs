﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempoBarPosition : MonoBehaviour {

	public Vector2 offset = Vector2.zero;
	public bool isIcon = false;
	public bool isHitPoint = false;

	private float barHeight = 0.0f;

	// Use this for initialization
	void Start () {

		offset.x = isIcon ? Mathf.Ceil (0.25f * Screen.width / PixelPerfectCamera.pixelsToUnits) : isHitPoint ? Mathf.Ceil (-0.18f * Screen.width / PixelPerfectCamera.pixelsToUnits) : offset.x;

		barHeight = Mathf.Ceil (Screen.height / PixelPerfectCamera.pixelsToUnits / 2 * 0.7f);
		GetComponent<TempoBarPosition> ().transform.position = new Vector3( 0 + offset.x, barHeight + offset.y, 0);

	}

}
