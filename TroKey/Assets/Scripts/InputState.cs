﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class InputState : MonoBehaviour {

	public float missThreshold = 32.0f;
	public float hitThreshold = 16.0f;

	public Sprite missSprite;
	public Sprite wooSprite;
	public Sprite geeSprite;

	private GameObject[] prefabs;
	private GameObject spawner;
	private List<string> keyPressList = new List<string> ();
	private static float hitPoint = 0.0f;

	void Awake(){
		spawner = GameObject.Find ("TempoSpawner");
		prefabs = spawner.GetComponent<Spawner> ().prefabs;
		Debug.Log (GetComponent<SpriteRenderer>().size);
	}

	void Start(){
		hitPoint = GetComponent<InputState> ().transform.position.x;
		Debug.Log (GetComponent<SpriteRenderer>().size);
	}

	// Update is called once per frame
	void Update () {
		var recycledScript = prefabs[0].GetComponent<RecycleGameObject> ();
		var pool = GameObjectUtil.GetObjectPool (recycledScript);
		var objectList = pool.GetActiveObjects ();
		if (Input.GetKeyDown (KeyCode.A)) {
			//Debug.Log ("press a");
			PressKey ("A", objectList);
		}
		if (Input.GetKeyDown (KeyCode.D)) {
			//Debug.Log ("press d");
			PressKey ("D", objectList);
		}
		objectList = pool.GetActiveObjects ();
		objectList.Sort (new MissIconComparator());
		if (objectList [0].GetComponent<TempoIconObject> ().hit == false && (hitPoint - objectList [0].transform.position.x) > missThreshold ) {
			objectList [0].GetComponent<TempoIconObject> ().hit = true;
			objectList [0].GetComponent<TempoIconObject> ().miss = true;
			objectList [0].GetComponent<SpriteRenderer> ().sprite = missSprite;
			Debug.Log ("not hit!");
		}
	}

	void PressKey(string input, List<RecycleGameObject> objectList){

		objectList.Sort (new TempoIconComparator());
//		Debug.Log (objectList [0].transform.position.x);
//		Debug.Log (objectList [1].transform.position.x);


		if (objectList [0].GetComponent<TempoIconObject> ().hit == false && (Mathf.Abs (objectList [0].transform.position.x - hitPoint) <= hitThreshold)) {
			objectList [0].GetComponent<TempoIconObject> ().hit = true;
			keyPressList.Add (input);
			if (input == "A")
				objectList [0].GetComponent<SpriteRenderer> ().sprite = wooSprite;
			else
				objectList [0].GetComponent<SpriteRenderer> ().sprite = geeSprite;
		} else if (objectList [0].GetComponent<TempoIconObject> ().hit == false && (Mathf.Abs (objectList [0].transform.position.x - hitPoint) > hitThreshold) && (Mathf.Abs (objectList [0].transform.position.x - hitPoint) <= missThreshold)) {
			objectList [0].GetComponent<TempoIconObject> ().hit = true;
			objectList [0].GetComponent<TempoIconObject> ().miss = true;
			objectList [0].GetComponent<SpriteRenderer> ().sprite = missSprite;
			Debug.Log ("miss!");
		}
		string tmp = "";
		foreach(string a in keyPressList)
			tmp += a;
		Debug.Log (tmp);
	}

	public class TempoIconComparator: IComparer<RecycleGameObject> {

		public int Compare(RecycleGameObject r1, RecycleGameObject r2) {
			return (int)(Mathf.Abs (r1.transform.position.x - hitPoint) - Mathf.Abs (r2.transform.position.x - hitPoint));
		}
	}

	public class MissIconComparator: IComparer<RecycleGameObject> {

		public int Compare(RecycleGameObject r1, RecycleGameObject r2) {
			return (int)(r1.transform.position.x - r2.transform.position.x);
		}
	}
}