﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempoBarBackground : MonoBehaviour {

	public int textureSize = 32;
	public bool scaleHorizontially = true;
	public bool scaleVertically = false;


	// Use this for initialization (After Awake)
	void Start () {
		var newWidth = !scaleHorizontially ? 1 : Mathf.Ceil ( 0.5f * Screen.width / (textureSize * PixelPerfectCamera.scale));
		var newHeight = !scaleVertically ? 1 : Mathf.Ceil (Screen.height / (textureSize * PixelPerfectCamera.scale));

		transform.localScale = new Vector3 (newWidth * textureSize, newHeight * textureSize, 1);

		GetComponent<Renderer> ().material.mainTextureScale = new Vector3 (newWidth, newHeight, 1);

	}
}
