﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour {

	public RecycleGameObject prefab;

	private List<RecycleGameObject> poolInstances = new List<RecycleGameObject>();

	private RecycleGameObject CreateInstance(Vector3 pos){

		var clone = GameObject.Instantiate (prefab);
		clone.transform.position = pos;
		clone.transform.parent = transform;

		poolInstances.Add (clone);

		return clone;
	}

	public RecycleGameObject NextObject(Vector3 pos){
		RecycleGameObject instance = null;

		foreach(var go in poolInstances){
			if (go.gameObject.activeSelf != true) {
				instance = go;
				instance.transform.position = pos;
				TempoIconObject tempoIcon = instance.GetComponent<TempoIconObject> ();
				if (tempoIcon) {
					tempoIcon.hit = false;
					tempoIcon.miss = false;
					tempoIcon.GetComponent<SpriteRenderer> ().sprite = tempoIcon.originSprite;
				}
			}
		}

		if(instance == null)
			instance = CreateInstance (pos);

		instance.Restart ();

		return instance;
	}

	public List<RecycleGameObject> GetActiveObjects(){
		List<RecycleGameObject> objectList = new List<RecycleGameObject>();

		foreach (var element in poolInstances) {
			if (element.gameObject.activeSelf == true && element.GetComponent<TempoIconObject> ().hit == false) {
				objectList.Add (element);
			}
		}

		return objectList;
	}
}